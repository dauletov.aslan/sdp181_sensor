package kz.test.sensorexample;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        List<Sensor> allSensors = sensorManager.getSensorList(Sensor.TYPE_ALL);
        for (int i = 0; i < allSensors.size(); i++) {
            Log.d("Hello", allSensors.get(i).getName());
        }
    }

    public void openAcceleration(View view) {
        startActivity(new Intent(MainActivity.this, AccelerationActivity.class));
    }

    public void openOrientation(View view) {
        startActivity(new Intent(MainActivity.this, OrientationActivity.class));
    }
}